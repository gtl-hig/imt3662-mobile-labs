# IMT3673 labs #

This repo contains example code and labs for IMT3673 mobile development course. 
The projects use Android Studio 3.0.1, Android SDK 26 and the NDK 1.6. 
The repo has been updated last in March 2018. 

## ThreadingDemo

Simple threading demonstration for using Thread class, Executor pools, and
communication between UI thread and background threads.


## Activity Transition Demo ##

First entry level application to demonstrate fundamentals of Android UI, basic concepts of Activity and Fragments, and how to manage the activity stack. You will learn here how to:

   * animating activity transitions
   * testing with JUnit
   * testing with Espresso


## Tiles Puzzle ##

This application demonstrates how to incorporate 3rd party library, how to use OpenCV manager to dynamically load shared libraries, and how to setup an Android Studio project from an existing Eclipse-style source tree. Demo for:

   * using external library dependencies
   * using OpenCV 3 
   * using Settings and SettingsActivity


## Hello JNI ##

This is a slightly modified version of the default HelloJNI demo shipped with the Android SDK. It showcases how to make a native method call into C from within Java, and how to pass primitive types from Java-world to C-world. 


## SFML example ##

This is a default project to test that your setup for NDK development with NativeActivities is correct. It uses [SFML](https://github.com/SFML/SFML) as the media layer, and C code to instantiate window and handle user events.

# Preference Example #

This is an example application showing how to use preferences in android.