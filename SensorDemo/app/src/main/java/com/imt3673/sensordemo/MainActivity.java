package com.imt3673.sensordemo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {

    private final float MIN_DIST = 50;
    private TextView tvPressure;
    private TextView tvTemperature;
    private TextView tvAltitudeByPressure;
    private TextView tvAltitudeByGps;

    private SensorManager mSensorManager;
    private Sensor mSensorTemperature;
    private Sensor mSensorPressure;

    private LocationManager mLocationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupUI();
        setupSensors();
    }


    private void setupSensors() {
        this.mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        this.mSensorPressure = this.mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        this.mSensorTemperature = this.mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);


        PressureListener l = new PressureListener();
        this.mSensorManager.registerListener(l, this.mSensorPressure, SensorManager.SENSOR_DELAY_NORMAL);

        this.mSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(final SensorEvent event) {
                final float temperature = event.values[0];
                tvTemperature.setText("" + temperature);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        }, this.mSensorTemperature, SensorManager.SENSOR_DELAY_NORMAL);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            return;
        }

        this.mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, MIN_DIST, new GpsListener());
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != 0) { return; }


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            this.mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, MIN_DIST, new GpsListener());


    }

    private void setupUI() {
        this.tvPressure = findViewById(R.id.tv_pressure);
        this.tvTemperature = findViewById(R.id.tv_temperature);
        this.tvAltitudeByPressure = findViewById(R.id.tv_altitude_by_pressure);
        this.tvAltitudeByGps = findViewById(R.id.tv_altitude_by_gps);
    }


    class GpsListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            tvAltitudeByGps.setText(Double.toString(location.getAltitude()));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // tracking the accuracy or change in GPS
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            // TODO deal with the user disabling GPS
        }
    }


    class PressureListener implements SensorEventListener {

        private float mOldPressure;


        @Override
        public void onSensorChanged(final SensorEvent event) {
            final float pressure = event.values[0];

            if (pressure > mOldPressure) {
                mOldPressure = pressure;
                tvPressure.setText("" + pressure);
                tvAltitudeByPressure.setText("" + SensorManager.getAltitude(1013.25f, pressure));
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    }
}
