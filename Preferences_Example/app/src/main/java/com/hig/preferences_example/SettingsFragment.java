package com.hig.preferences_example;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;


/**
 *
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    //static keys, same as the one in preference.xml
    static final String keyEditText = "key_editText";

    //The keys can also be added to strings.xml but make sure not to change there value when localizing

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //inflate fragment, same as setting layout
        addPreferencesFromResource(R.xml.preferences);
    }

    public SettingsFragment() {    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals(keyEditText)) {

            //update summary text in the preference menu
            Preference editTextPref = findPreference(key);
            editTextPref.setSummary(sharedPreferences.getString(key,"default"));
        }
    }
}
