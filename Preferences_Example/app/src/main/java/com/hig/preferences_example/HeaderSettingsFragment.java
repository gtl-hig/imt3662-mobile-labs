package com.hig.preferences_example;


import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;

public class HeaderSettingsFragment extends PreferenceFragment {

    static final String KEY = "Key";
    static final String HEADER_ONE = "category_one";
    static final String HEADER_TWO = "category_two";

    public HeaderSettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get values from extra tag, see preference_headers.xml
        String settings = getArguments().getString(KEY);
        if(settings.equals(HEADER_ONE)){
            addPreferencesFromResource(R.xml.preference_one);
        }

        if(settings.equals(HEADER_TWO)){
            addPreferencesFromResource(R.xml.preference_two);
        }
    }
}
