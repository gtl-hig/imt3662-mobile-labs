package com.hig.preferences_example;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize settings
        //Should be done at the very start of the program. Will only happen once the first time the app starts.
        //Using true instead of false in the third parameter the function will set values to default overriding
        //any settings the user has specified
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        //Get settings value
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String text = preferences.getString(SettingsFragment.keyEditText, "Default");

        //Set text in text view.
        TextView textView = (TextView) findViewById(R.id.TxtInfo);
        textView.setText(text);

        //Setup buttons that go to settings
        Button settings = (Button) findViewById(R.id.BtnSettings);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToSettings();
            }
        });

        Button headerSettings = (Button) findViewById(R.id.BtnHeaderSettings);
        headerSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToHeaderSettings();
            }
        });

    }

    private void changeToSettings(){
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    private void changeToHeaderSettings(){
        Intent i = new Intent(this, HeaderSettingsActivity.class);
        startActivity(i);
    }
}
