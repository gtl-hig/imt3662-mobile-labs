package com.imt3673.threadingdemo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SecondActivity extends AppCompatActivity {


    private TextView tvText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        this.tvText = findViewById(R.id.tv_text2);
        Log.i("SecondActivity", "UI thread: " + Thread.currentThread().toString()
          + " id:" + Thread.currentThread().getId());

        final ExecutorService es = Executors.newSingleThreadExecutor();
        es.execute(() -> {
            // the delay at the start, has an impact
            // as long as it is longer than 50ms
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.i("SecondActivity",  "thread: " + Thread.currentThread().toString()
                    + " id:" + Thread.currentThread().getId());

            tvText.setText("Hello from thread");
            // the delay after has no impact
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

}
