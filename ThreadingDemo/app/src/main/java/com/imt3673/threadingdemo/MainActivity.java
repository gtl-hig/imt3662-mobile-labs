package com.imt3673.threadingdemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

// Demo for threading
public class MainActivity extends AppCompatActivity {

    private TextView tv;
    private EditText mEditText;
    private volatile AtomicInteger counter = new AtomicInteger(0);
    private final static int COUNTER_MAX = 100;

    // This variable behaves as volatile by default
    private volatile AtomicInteger jobCounter = new AtomicInteger(0);
    // volatile, but not atomic counter
    private volatile int jobCounter2 = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tv = findViewById(R.id.tv_hello);
        this.mEditText = findViewById(R.id.editText);

        final ExecutorService es = Executors.newFixedThreadPool(8);

        for (int i = 0; i < 100; i++) {
            es.execute(() -> {
                while (counter.getAndIncrement() < COUNTER_MAX) {
                    doSomeWork();
                }
            });
        }

        es.execute(() -> {
            while (counter.get() < COUNTER_MAX) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            tv.post(() -> {
                tv.setText("Jobs done, " + jobCounter + "(" + jobCounter2 + ")" + " by attempts " + counter.get());
            });
        });
    }

    private void doSomeWork() {
        jobCounter.incrementAndGet();

        int v = jobCounter2;
        v++;
        jobCounter2 = v;

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startSecondActivity(View view) {
        final Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }

    public void startThreadingTests(View view) {
        final int TEST_COUNT = 100;
        long start = System.currentTimeMillis();
        for (int i = 0; i < TEST_COUNT; i++) {
            new Thread(() -> {
            }).start();
        }
        long end = System.currentTimeMillis();
        mEditText.setText(mEditText.getText() +
                "\nTime to create " + TEST_COUNT + " threads: " + (end - start));


        // start = System.currentTimeMillis();
        final ExecutorService es = Executors.newFixedThreadPool(1000);
        for (int i = 0; i < TEST_COUNT; i++) {
            es.execute(() -> {
            });
        }

        start = System.currentTimeMillis();
        for (int i = 0; i < TEST_COUNT; i++) {
            es.execute(() -> {
            });
        }
        end = System.currentTimeMillis();
        mEditText.setText(mEditText.getText() +
                "\nTime with ThreadPool " + (end - start));


        new Thread(() -> {
            long start1 = System.currentTimeMillis();
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long end1 = System.currentTimeMillis();
            mEditText.post(() -> {
                mEditText.setText(mEditText.getText() +
                        "\nTime in sleep [ms] " + (end1 - start1));
            });

        }).start();
    }
}
